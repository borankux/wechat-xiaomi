<?php
/**
 * Created by PhpStorm.
 * User: mirzat
 * Date: 2018/10/21
 * Time: 上午12:13
 */

namespace App\Http\Controllers;


use App\Models\RequestLog;
use App\Utils\IpUtil;
use Illuminate\Http\Request;

class IndexController
{
    public function index(Request $request)
    {
        //todo record ip address
        //todo record user agent
//        $data['headers'] = $request->headers;
//        $data['server'] = $request->server;
//        $data['payload'] = $request->all();
        $ip = $request->ip();
        $data['ip'] = $ip;
        $data['user-agent'] = $request->userAgent();
        $data['method'] = $request->method();
        $data['url'] = $request->url();
        $data['country'] = IpUtil::country($ip)['country'];
        $data['city'] = IpUtil::country($ip)['city'];
        RequestLog::create($data);
        return json_encode('welcome friend, now i have got your finger print');
    }
}