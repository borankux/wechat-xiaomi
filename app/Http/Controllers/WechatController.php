<?php 

namespace App\Http\Controllers;

use App\Http\Handlers\GeneralReplyHandler;
use EasyWeChat;
use Log;

class WechatController extends Controller
{
    protected $app;
	public function index()
	{
	    return $this->ok([
	        'status' => 'online'
        ]);
	}

	public function serve()
    {
        $app = EasyWechat::officialAccount();

        //add handler
        $app->server->push(GeneralReplyHandler::class);

        return $app->server->serve();
    }
}