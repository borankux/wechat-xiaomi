<?php

namespace App\Http\Controllers;

use App\Http\Responses\ApiResponse;
use Illuminate\Http\Request;

class DemoController extends Controller
{
    use ApiResponse;

    public function __construct()
    {

    }

    public function demo(Request $request)
    {
        $phone = $request->get('phone');
        $data = [
            'phone' => $phone
        ];
        return $this->ok($data);
    }
}
