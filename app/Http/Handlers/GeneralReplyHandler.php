<?php
/**
 * Created by PhpStorm.
 * User: mirzat
 * Date: 2018/10/21
 * Time: 上午4:26
 */

namespace App\Http\Handlers;

use App\QParser;
use EasyWeChat\Kernel\Contracts\EventHandlerInterface;
use EasyWeChat\Kernel\Messages\NewsItem;
use EasyWeChat\Kernel\Messages\Voice;
use Log;

class GeneralReplyHandler implements EventHandlerInterface
{

    /**
     * @param array $payload
     * @return mixed
     */
    public function handle($payload = null)
    {
        if($payload['MsgType']=='voice') {
//            return $payload['Recognition'];
            return new Voice($payload['MediaId']);
        }
        return 'under develop';
    }
}