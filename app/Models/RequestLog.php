<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RequestLog extends Model
{
    public $fillable = [
        'url',
        'ip',
        'method',
        'method',
        'user-agent',
        'country'
    ];
}
