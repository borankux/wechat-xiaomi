<?php
/**
 * Created by PhpStorm.
 * User: mirzat
 * Date: 2018/10/21
 * Time: 上午6:27
 */

namespace App\Utils;

class QParser
{

    const QUESTION_SCHEMA_PATH = 'questionnaires';


    public static function parse($questionName)
    {
        $schema = file_get_contents(base_path(self::QUESTION_SCHEMA_PATH.'/'.$questionName.'.json'));
        return json_decode($schema);
    }
}