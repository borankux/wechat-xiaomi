<?php
/**
 * Created by PhpStorm.
 * User: mirzat
 * Date: 2018/12/11
 * Time: 6:25 AM
 */

namespace App\Utils;


use GuzzleHttp\Client;

class IpUtil
{
    public static function country(string $ip) {
        //https://ipfind.co
        $url = "https://ipfind.co?ip=";
        $client = new Client();
        $data = ['country'=>null,'city'=>null];
        try {
            $res = $client->get($url.$ip)->getBody();
            $response = json_decode($res);
            $data['country'] = $response->country;
            $data['city'] = $response->city;            
        } catch (\Exception $ex) {
            \Log::error(json_encode($ex));
        }
        return $data;
    }
}