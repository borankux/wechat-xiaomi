<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


//认证相关

Route::get('demo', 'DemoController@demo');

Route::get('wechat', 'WechatController@index');
Route::any('wechat/serve', 'WechatController@serve');
